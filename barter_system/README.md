# Barter System
Énoncé: https://csacademy.com/ieeextreme12/task/barter-system/

In a world where currency is not available, people fulfill their necessities by exchanging commodities among them. However, not all commodities are equal.

Exchange rate of commodities *A* to *B* is defined as the number of units of *B* one can get with one unit of *A*.

Given exchange rates of a set of pairs of commodities as input, answer a set of queries by finding the exchange rate of a specific pair.

## Standard Input
The first line of the input is an integer *N* which indicates the number of given exchange rates to follow. The next *N* lines consists of *A,B,r* triplets where *A* and *B* are the commodities and *r* is the exchange rate such that *A=r⋅B (mod 998244353)*.

The next line contains another integer *Q* which represents the number of queries. The following *Q* lines consists of a pair of commodities *K* and *L*.

## Standard Output
For each of the *Q* queries you need to find *r* such that *K=r⋅L*.

It can be shown that *r* can be represented as *X/Y*, where *X* and *Y* are coprime integers and *X != 0 (mod 998244353)*. For each query print *X⋅Y^−1 (mod 998244353)*.

If the exchange rate can not be computed using the given information, print `-1`.

## Constraints and Notes
 - 1 <= N, Q <= 2⋅10^4
 - 1 <= ∣A∣,∣B∣ <= 50
 - 1 <= r < 9982443531
 - All exchange rates are consistent

## Example

|Input|Output|Explanation|
|-----|------|-----------|
|Voir [barter.in](./barter.in)|279508419<br>-1<br>1|Rice = *399297742⋅598946612* Beef, which is *279508419*.<br><br>There is not enough information to convert from Beef to Banana, so `-1` is printed.|
