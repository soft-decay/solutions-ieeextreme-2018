#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import sys

def ext_euclid(a, mod):
    return _egcd(mod, a, 1, 0, mod)

def _egcd(a, b, c, d, mod):
    if b == 1:
        return c, b
    else:
        if b == 0:
            return float("nan"), a
        return _egcd(b, a % b, (d - c * (a // b)) % mod, c, mod)


def find_rate(start, end, visited):
    if end in rates_table[start]:
        return rates_table[start][end]
    elif start == end:
        return 1
    else:
        visited.add(start)
        for elem in rates_table[start]:
            if elem not in visited:
                rate = (rates_table[start][elem] * find_rate(elem, end, visited)) % 998244353
                if rate != 0:
                    return rate
    return 0


num_rates = int(sys.stdin.readline().strip("\n"))

lines = [line.strip("\n").split(" ") for line in sys.stdin.readlines()]
rates = lines[:num_rates]
num_queries = int(lines[num_rates][0])
queries = lines[num_rates + 1:]

rates_table = {}
for rate in rates:
    if rate[0] not in rates_table:
        rates_table[rate[0]] = {}
    if rate[1] not in rates_table:
        rates_table[rate[1]] = {}
    rates_table[rate[1]][rate[0]] = ext_euclid(int(rate[2]), 998244353)[0]
    rates_table[rate[0]][rate[1]] = int(rate[2])

for query in queries:
    visited = set()
    r = find_rate(query[0], query[1], visited)
    if r == 0:
        print(-1)
    else:
        print(r)
