#include <iostream>
#include <string>
#include <cstring>

using namespace std;

struct Noeuds {
   int contenu;
   Noeuds* gauche = NULL;
   Noeuds* droite = NULL;
   int h;
   Noeuds();
   Noeuds(int _contenu, int _h){
       contenu = _contenu;
       h = _h;
   }
   ~Noeuds(){
       if(gauche != NULL) delete gauche;
       if(droite != NULL) delete droite;
   }
};

class File{
  public:
    File();
    ~File();

    void enfiler(Noeuds*);
    Noeuds* defiler();
    bool  vide() const {return queue==NULL;}

  private:
    class Cellule{
      public:
        Cellule( Noeuds* e, Cellule* c=NULL);
        Noeuds* element;
        Cellule* suivante;
    };

    Cellule *queue;
};


int chercher(const char&);
void add(Noeuds*&, int index, int hauteur);
void printTable(Noeuds* racine);

char tabI[28];
char tabP[28];
Noeuds* racine;
int taille;

int main() {
    do{
        cin.getline(tabI, 28);
        cin.getline(tabP, 28);
        taille = strlen(tabI);
        for(int i = 0; i < taille; ++i){
            add(racine, chercher(tabP[i]), 0);
        }
        printTable(racine);
        cout << '\0';
        cout << "\n";
        delete racine;
        racine = NULL;
    } while(!cin.eof());
    return 0;
}


void add(Noeuds*& noeud, int c, int h){
    if(noeud == NULL){
        noeud = new Noeuds(c, h); 
    } else if (c < noeud->contenu) {
        ++h;
        add(noeud->gauche, c, h);
    } else {
        ++h;
        add(noeud->droite, c, h);
    }
}

void printTable(Noeuds* racine){
    if(racine == NULL) return;
    File file;
    file.enfiler(racine);
    int hauteur = 0;
    int dernier = 0;
    while(!file.vide()){
        Noeuds* n = file.defiler();
        if(hauteur != n->h) {
            cout << endl;
            ++hauteur;
            dernier = 0;
        } 
        for (int i = dernier; i < n->contenu; i++) {
            cout << " ";
        }
        dernier = n->contenu + 1;
        cout << tabI[n->contenu]<<'\0';
        if(n->gauche != NULL){
            file.enfiler(n->gauche);
        }
        if(n->droite != NULL){
            file.enfiler(n->droite);
        }
    }
}

int chercher(const char& a){
    for(int i = 0;i < taille; ++i) {
        if(tabI[i] == a){
            return i;
        }
    }
    return -1;
}

File::File(){
    queue = NULL;
}

File::~File(){
    while(queue != NULL){
        defiler();
    }
}

File::Cellule::Cellule(Noeuds* e, Cellule* c)
: element(e), suivante(c){}

void File::enfiler(Noeuds* e)
{
    if(queue == NULL){
        queue = new Cellule(e);
    } else {
        Cellule *tmp = queue;
        while(tmp->suivante != NULL){
            tmp = tmp->suivante;
        }
        tmp->suivante = new Cellule(e);
    }
}

Noeuds* File::defiler()
{
    Noeuds* result = queue->element;
    Cellule* tmp = queue;
    queue = queue->suivante;
    delete tmp;
    return result;
}
